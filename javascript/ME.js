/**
 * ME.js
 * (Mechanical Engineering)
 */
/*========================================================================================*/
var Glob = {
    canvas_id : "MyCanvas",
    canvas_width : 600,
    canvas_height : 500,
    canvas_background_color : "gray",
    lager_schraffur_laenge : 4,
    festlager_spreizwinkel : 30, // degree
    festlager_radius : 6, // Radius des runden (kreisförmigen) Kopfes
};

/*========================================================================================*/
class Point {
    constructor (x,y) {
        this.x = x;
        this.y = y;
    }
}

class Punkt extends Point {
    constructor (x,y) {
        super(x,y);
    }
}

/*========================================================================================*/
var Origin = function () {
    this.canvas = document.getElementById(Glob.canvas_id);
    this.context = this.canvas.getContext('2d');
    /*
     *
     *            1/2 * PI
     *                |
     *                |
     *     PI ________o_________ 0
     *                |         (2 PI)
     *                |
     *            3/2 * PI
     * 
     * Drawing process starts by 0 and continues in clockwise/anticlockwise directorion.
     *
     */
    this.DrawLineByAngle = function (startX, startY, /* radian */ angle, length, clockwise=false) {    
        this.context.beginPath();
        this.context.moveTo(startX, startY);
        var EndPoint = this.GetEndPointOfLine(startX, startY, angle, length, clockwise);
        this.context.lineTo(EndPoint.x, EndPoint.y);
        this.context.closePath();
        this.context.stroke();
    };
    this.DrawLineByPoints = function (startX, startY, endX, endY) {
        this.context.beginPath();
        this.context.moveTo(startX, startY);
        this.context.lineTo(endX, endY);
        this.context.closePath();
        this.context.stroke();
    };
    this.DrawRectByPoints = function (startX, startY, endX, endY) {
        if (startX == endX || startY == endY) {
            this.DrawLineByPoints(startX, startY, endX, endY);
        }
        else {
            var oRectHeight = Math.abs(startY - endY);
            var oRectWidth = Math.abs(startX - endX);
            this.context.rect(startX, startY, oRectWidth, oRectHeight);
        }
    };
    this.GetEndPointOfLine = function (startX, startY, /* radian */ angle, length, clockwise=false) {
        var MathPIHalf = Math.PI / 2; // 90°
        var MathPIDouble = Math.PI * 2; // 360°
        var Dx = 0, Dy = 0;
        var endX = 0;
        var endY = 0;
        // Calculate angle and end points
        if (angle < MathPIHalf) { // < 90°
            Dx = length * Math.cos(angle);
            Dy = length * Math.sin(angle);
            endX = startX + Dx;
            endY = (clockwise) ? (startY + Dy) : (startY - Dy);
        }
        else if (angle == MathPIHalf) {
            endX = startX;
            endY = (clockwise) ? (startY + length) : (startY - length);
        }
        else if (angle > MathPIHalf && angle < Math.PI) {
            Dx = length * Math.cos(Math.PI - angle);
            Dy = length * Math.sin(Math.PI - angle);
            endX = startX - Dx;
            endY = (clockwise) ? (startY + Dy) : (startY - Dy);
        }
        else if (angle == Math.PI) {
            endX = startX - length;
            endY = startY;
        }
        else if (angle > Math.PI && angle < (3 * MathPIHalf)) {
            Dx = length * Math.cos(angle - Math.PI);
            Dy = length * Math.sin(angle - Math.PI);
            endX = startX - Dx;
            endY = (clockwise) ? (startY - Dy) : (startY + Dy);
        }
        else if (angle == (3 * MathPIHalf)) {
            endX = startX;
            endY = (clockwise) ? (startY - length) : (startY + length);
        }
        else if (angle > (3 * MathPIHalf) && angle < MathPIDouble) {
            Dx = length * Math.cos(MathPIDouble - angle);
            Dy = length * Math.sin(MathPIDouble - angle);
            endX = startX + Dx;
            endY = (clockwise) ? (startY - Dy) : (startY + Dy);
        }
        else if (angle == MathPIDouble || angle == 0) {
            endX = startX + length;
            endY = startY;
        }
        return { x : endX, y : endY };
    };
    this.ConvertDegreeToRadian = function (deg) {
        return (deg / 180 * Math.PI);
    };
};
/*========================================================================================*/
/* class Komponente {
    constructor(typ, name) {
        Origin.call(this);
        this.typ = typ;
        this.name = name;
        this.SetPosition = function (x, y) {
            this.x = x;
            this.y = y;
        };
    }
} */

var Komponente = function (typ, name) {
    Origin.call(this);
    this.typ = typ;
    this.name = name;
    this.SetPosition = function (x, y) {
        this.x = x;
        this.y = y;
    };
};

/*========================================================================================*/
var Lager = function (wertigkeit, typ, name) {
        Komponente.call(this, typ, name);
        this.wertigkeit = wertigkeit; // default
        /*
        * @params
        *      {rotateAngle}: in degree
        *      {strokeStyle}
        */
        this.Paint = function (rotateAngle = 270 /* degree */, strokeStyle = "#FFF") {
            this.context.strokeStyle = strokeStyle;
            var mult_faktor = 5;

            var line_length = Glob.festlager_radius * mult_faktor; // Laenge der zwei Spreizbeine
            var EPOL = null; // Endpoints of line
            var bottomStart = { x : 0, y : 0 };      
            var bottomEnd = { x : 0, y : 0 };

            var flwL = rotateAngle + Glob.festlager_spreizwinkel; /* Festlagerwinkel Bein links */
            var flwR = rotateAngle - Glob.festlager_spreizwinkel; /* Festlagerwinkel Bein rechts */
            flwL =  this.ConvertDegreeToRadian(flwL); // degree => radian
            flwR =  this.ConvertDegreeToRadian(flwR);

            // Draw circle
            this.context.beginPath();
            this.context.arc(this.x, this.y, Glob.festlager_radius, 0, 2 * Math.PI);
            this.context.closePath();
            this.context.stroke();

            // Draw line 1 (left/down)
            EPOL = this.GetEndPointOfLine(this.x, this.y, flwL, Glob.festlager_radius);
            this.DrawLineByAngle(EPOL.x, EPOL.y, flwL, line_length);
            EPOL = this.GetEndPointOfLine(EPOL.x, EPOL.y, flwL, line_length);
            bottomStart.x = EPOL.x;
            bottomStart.y = EPOL.y;

            // Draw line 2 (right/up)
            EPOL = this.GetEndPointOfLine(this.x, this.y, flwR, Glob.festlager_radius);
            this.DrawLineByAngle(EPOL.x, EPOL.y, flwR, line_length);
            EPOL = this.GetEndPointOfLine(EPOL.x, EPOL.y, flwR, line_length);
            bottomEnd.x = EPOL.x;
            bottomEnd.y = EPOL.y;        

            // Draw line 3 (bottom)
            this.DrawLineByPoints(bottomStart.x, bottomStart.y, bottomEnd.x, bottomEnd.y);

            // Draw line 4 (double bottom line) for Loslager
            var GG = 4; // Gap between the two bottom lines for Loslager
            var RotateAngle = { sin : Math.sin(this.ConvertDegreeToRadian(rotateAngle)), 
                                cos : Math.cos(this.ConvertDegreeToRadian(rotateAngle)) };
            if ( this.wertigkeit == 1 && this.typ == "Loslager" ) {
                bottomStart.x += RotateAngle.cos * GG;
                bottomStart.y -= RotateAngle.sin * GG;
                bottomEnd.x += RotateAngle.cos * GG;
                bottomEnd.y -= RotateAngle.sin * GG;
                this.DrawLineByPoints(bottomStart.x, bottomStart.y, bottomEnd.x, bottomEnd.y);
            }

            // Draw hatching (Schraffuren)
            var BL = 2 * Math.ceil((line_length + Glob.festlager_radius) * Math.sin(this.ConvertDegreeToRadian(Glob.festlager_spreizwinkel))); // Bottom line's length
            var DD = 4; // hatching (schraffur) length
            var currentX = bottomStart.x, currentY = bottomStart.y;
            var hatchingX = 0, hatchingY = 0;        
            for (var i = 0; i <=  BL; i += DD) {
                hatchingX = currentX + RotateAngle.cos * DD * 1.5;
                hatchingY = currentY - RotateAngle.sin * DD * 1.5;
                this.DrawLineByPoints(currentX, currentY, hatchingX, hatchingY);
                // Update coordinate
                currentX += RotateAngle.sin * DD;
                currentY += RotateAngle.cos * DD;
            }
        };
};


var Loslager = function () {
    Lager.call(this, 1, "Loslager", "");
};


var Festlager = function () {
    Lager.call(this, 2, "Festlager", "");
    
    
};

/*========================================================================================*/
var Verbindung = function (anzahlKraftUebertragung, typ, name) {
    Komponente.call(this, typ, name);
    this.anzahlKraftUebertragung = 0; // default
};

var Stab = function () {
    Verbindung.call(this, 1, "Stab", "");
};

var Gelenk = function () {
    Verbindung.call(this, 2, "Gelenk", "");
};

/*========================================================================================*/
var Koerper = function (typ, name) {
    Komponente.call(this, typ, name);
};

var Rechteck = function (laenge, breite) { // Scheibe
    Koerper.call(this, "Rechteck", "");
    this.eckpunkte = [];
};

var Balken = function (startX = 0, startY = 0, endX = 0, endY = 0) {
    Koerper.call(this, "Balken", "");
    this.breite = 8; // const
    this.startpunkt = { x : startX, y : startY }
    this.endpunkt = { x : endX, y : endY }
    this.zwischenpunkte = [];
    this.AddZwischenpunkt = function (x, y) {
        this.zwischenpunkte.push(new Punkt(x,y));
    };
    this.GetFirstZwischenpunkt = function () {
        return this.zwischenpunkte.shift();
    };
    this.GetLastZwischenpunkt = function () {
        return this.zwischenpunkte.pop();
    };
    this.Paint = function () {
        if (this.zwischenpunkte.length == 0) {
            this.DrawRectByPoints(this.startpunkt.x, this.startpunkt.y, this.endpunkt.x, this.endpunkt.y);
        }
    };
};

/*========================================================================================*/
var Tragwerk = function () {
    //this.komponenten = { lagers : [], verbindungen : [], gelenke : [] };
};

/*========================================================================================*/
var Program = function () {
    Origin.call(this);
    this.Init = function () {
        this.canvas.style.backgroundColor = Glob.canvas_background_color;
        this.canvas.width = Glob.canvas_width;
        this.canvas.height = Glob.canvas_height;
    };
};

/*========================================================================================*/
var m_Program = new Program();
m_Program.Init();

var m_Loslager = new Loslager();
var m_Festlager = new Festlager();
var m_Festlager2 = new Festlager();

m_Loslager.SetPosition(100,150);
m_Loslager.Paint();

m_Festlager.SetPosition(50,50);
m_Festlager.Paint(290);

m_Festlager2.SetPosition(150,50);
m_Festlager2.Paint(50);

var m_Punkt = new Punkt(3,2);
console.log("Punkt: (" + m_Punkt.x + "," + m_Punkt.y + ")");

var m_Balken = new Balken(5, 10, 50, 10);
m_Balken.Paint();